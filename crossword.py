inp = '''WWWWWWWWBWWWWWW
WBWBWBWBBBWBWBW
WWWWWWWWBWWWWWW
WBWBWBWBWBWBWBW
WWWWWWWWWWWBBBB
WBWBBBWBWBWBWBW
WWWWWBWWWWWWWWW
WBWBWBWBWBWBWBW
WWWWWWWWWBWWWWW
WBWBWBWBWBBBWBW
BBBBWWWWWWWWWWW
WBWBWBWBWBWBWBW
WWWWWWBWWWWWWWW
WBWBWBBBWBWBWBW
WWWWWWBWWWWWWWW'''

def list_boxes(cross: str) -> list[str]:
    return cross.split('\n')

def crossword(boxes: list[list[str]]) -> None:
    assigned = []
    clue = 0
    for row_index, row in enumerate(boxes):
        for col_index, char in enumerate(row):
            if char == 'W':
                if (col_index == 0 or boxes[row_index][col_index - 1] == 'B') and \
                   (col_index + 1 < len(row) and boxes[row_index][col_index + 1] == 'W'):
                    assigned.append((row_index, col_index, clue))
                    clue += 1
                    
                
                if (row_index == 0 or boxes[row_index - 1][col_index] == 'B') and \
                   (row_index + 1 < len(boxes) and boxes[row_index + 1][col_index] == 'W'):
                    assigned.append((row_index, col_index, clue))
                    clue += 1
    assigned.remove((0, 0, 0))
    return assigned

print(crossword(inp.split('\n')))
